cmake_minimum_required(VERSION 3.6)
project(passgen)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY  "${CMAKE_BINARY_DIR}/bin/")

set(SOURCE_FILES src/main.cpp src/Mask.cpp src/Mask.h src/Constants.h
        src/MarkovPassGen.cpp src/MarkovPassGen.h src/Runner.cpp src/Runner.h
        src/PassGen.h)

if (WIN32)
    include_directories(${CMAKE_SOURCE_DIR}/include/windows)
endif(WIN32)

file(COPY "${CMAKE_SOURCE_DIR}/rockyou.wstat" DESTINATION
        "${CMAKE_CURRENT_BINARY_DIR}/bin/")

add_executable(passgen ${SOURCE_FILES})
