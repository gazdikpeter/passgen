/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    21/02/17
 *  @copyright The MIT License (MIT)
 *
 */


#ifndef PASSGEN_CONSTANTS_H
#define PASSGEN_CONSTANTS_H

const unsigned MIN_PASS_LENGTH = 1;
const unsigned MAX_PASS_LENGTH = 64;
const unsigned CHARSET_SIZE = 256;

#endif //PASSGEN_CONSTANTS_H
