/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    21/02/17
 *  @copyright The MIT License (MIT)
 *
 */

#include "MarkovPassGen.h"

#include <algorithm>
#include <sstream>

#ifdef _WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#include <cstring>
#include <iostream>

#endif

const unsigned ETX = 3;

using namespace std;

MarkovPassGen::~MarkovPassGen()
{

}

MarkovPassGen::MarkovPassGen(const MarkovPassGen::Options &options) :
        _statFile { options.statFile }, _mask { options.mask }
{
    _thresholds = vector<unsigned>(MAX_PASS_LENGTH, 0);
    _permutations = vector<uint64_t>(MAX_PASS_LENGTH + 1, 0);

    parseOptions(options);

    // Determine maximal threshold
    _maxThreshold = *max_element(_thresholds.begin(), _thresholds.end());

    // Set current length
    _currentLength = _minLength;

    // Initialize memory
    initMemory();

    _index = _permutations[_minLength - 1];
    _lastIndex = _permutations[_maxLength];
}

void MarkovPassGen::initMemory()
{
    // Calc the permutations for each length
    _permutations[0] = 0;
    for (int i = 1; i < MAX_PASS_LENGTH + 1; i++)
    {
        _permutations[i] = _permutations[i - 1] + numOfPasswords(i);
    }

    // Open the file with a markov statistics
    ifstream input(_statFile, ifstream::in | ifstream::binary);
    unsigned statLength = skipHeader(input);

    /**
     * Create the Markov table from statistics
     */

    // Prealocate a memory to store statistics
    const unsigned markovMatrixSize = CHARSET_SIZE * CHARSET_SIZE *
                                      MAX_PASS_LENGTH;

    uint16_t *markovMatrixBuffer = new uint16_t[markovMatrixSize];
    uint16_t *markovMatrix[MAX_PASS_LENGTH][CHARSET_SIZE];
    auto markovMatrixPtr = markovMatrixBuffer;

    for (unsigned p = 0; p < MAX_PASS_LENGTH; p++) {
        for (unsigned i = 0; i < CHARSET_SIZE; i++) {
            markovMatrix[p][i] = markovMatrixPtr;
            markovMatrixPtr += CHARSET_SIZE;
        }
    }

    // Read statistics into the prealocated buffer
    input.read(reinterpret_cast<char *>(markovMatrixBuffer), statLength);

    // If the classic Markov model is defined duplicate statistics for first
    // position to each position
    if (_model == Model::CLASSIC) {
        markovMatrixPtr = markovMatrixBuffer;
        for (unsigned p = 1; p < MAX_PASS_LENGTH; p++) {
            markovMatrixPtr += CHARSET_SIZE * CHARSET_SIZE;

            memcpy(markovMatrixPtr, markovMatrixBuffer, CHARSET_SIZE *
                   CHARSET_SIZE * sizeof(uint16_t));
        }
    }

    // Convert values from network byte order to host byte order
    for (unsigned i = 0; i < markovMatrixSize; i++) {
        markovMatrixBuffer[i] = ntohs(markovMatrixBuffer[i]);
    }

    // Create the sort table to order characters in Markov table by
    // its probability
    SortElement *markovSortTableBuffer = new SortElement[markovMatrixSize];
    SortElement *markovSortTable[MAX_PASS_LENGTH][CHARSET_SIZE];
    auto markovSortTablePtr = markovSortTableBuffer;

    for (unsigned p = 0; p < MAX_PASS_LENGTH; p++) {
        for (unsigned i = 0; i < CHARSET_SIZE; i++) {
            markovSortTable[p][i] = markovSortTablePtr;
            markovSortTablePtr += CHARSET_SIZE;
        }
    }

    // Fill the sort table
    for (unsigned p = 0; p < MAX_PASS_LENGTH; p++) {
        for (unsigned i = 0; i < CHARSET_SIZE; i++) {
            for (unsigned j = 0; j < CHARSET_SIZE; j++) {
                markovSortTable[p][i][j].nextState = static_cast<uint8_t>(j);
                markovSortTable[p][i][j].probability = markovMatrix[p][i][j];
            }
        }
    }

    // Apply the mask
    applyMask(markovSortTable);

    // Order elements by its probability
    for (unsigned p = 0; p < MAX_PASS_LENGTH; p++) {
        for (unsigned i = 0; i < CHARSET_SIZE; i++) {
            qsort(markovSortTable[p][i], CHARSET_SIZE, sizeof(SortElement),
                  compareSortElements);
        }
    }

    // Create the final Markov table
    _markovTableSize = _maxLength * CHARSET_SIZE * _maxThreshold;
    _markovTable = new char[_markovTableSize];

    // Copy values from the sort table into it
    unsigned index, index1, index2;
    for (unsigned p = 0; p < _maxLength; p++) {
        index1 = p * CHARSET_SIZE * _maxThreshold;
        for (unsigned i = 0; i < CHARSET_SIZE; i++) {
            index2 = i * _maxThreshold;
            for (unsigned j = 0; j < _maxThreshold; j++) {
                index = index1 + index2 + j;
                _markovTable[index] = markovSortTable[p][i][j].nextState;
            }
        }
    }

    delete[] markovMatrixBuffer;
    delete[] markovSortTableBuffer;
}

void MarkovPassGen::parseOptions(const MarkovPassGen::Options &options)
{
    stringstream ss;
    string substr;

    // Parse length values
    ss << options.length;
    std::getline(ss, substr, ':');
    _minLength = stoi(substr);
    std::getline(ss, substr);
    _maxLength = stoi(substr);

    /**
     * Parse threshold values
     */
    ss.clear();
    ss << options.thresholds;

    // The global threshold
    std::getline(ss, substr, ':');
    for (unsigned i = 0; i < MAX_PASS_LENGTH; i++)
    {
        _thresholds[i] = stoi(substr);
    }

    // Adjust the threshold values according to the mask
    for (int i = 0; i < MAX_PASS_LENGTH; i++)
    {
        unsigned maskCharacters = _mask[i].Count();

        if (maskCharacters < _thresholds[i])
            _thresholds[i] = maskCharacters;
    }

    // The positional thresholds
    unsigned i = 0;
    while (std::getline(ss, substr, ','))
    {
        _thresholds[i] = stoi(substr);
        i++;
    }

    // The type of Markov model
    if (options.model == "classic")
        _model = Model::CLASSIC;
    else if (options.model == "layer")
        _model = Model::LAYERED;
    else
        throw invalid_argument("The command line argument 'model' has invalid value.");
}

int MarkovPassGen::compareSortElements(const void *p1, const void *p2)
{
    const SortElement *e1 = static_cast<const SortElement *>(p1);
    const SortElement *e2 = static_cast<const SortElement *>(p2);

    int characterOrder = e2->nextState - e1->nextState;
    int probabilityOrder = e2->probability - e1->probability;

    // Only the second element contains a valid character
    if (not isValidChar(e1->nextState) && isValidChar(e2->nextState))
        return 1;

    // Only the first element contains a valid character
    if (isValidChar(e1->nextState) && not isValidChar(e2->nextState))
        return -1;

    // Both elements contain an invalid character
    if (not isValidChar(e1->nextState) && not isValidChar(e2->nextState))
        return characterOrder;

    return probabilityOrder != 0 ? probabilityOrder : characterOrder;
}

bool MarkovPassGen::isValidChar(uint8_t value)
{
    return (value >= 32) ? true : false;
}

uint64_t MarkovPassGen::numOfPasswords(unsigned length)
{
    uint64_t result = 1;

    for (unsigned i = 0; i < length; i++)
        result *= _thresholds[i];

    return result;
}

unsigned MarkovPassGen::skipHeader(std::ifstream &statFile)
{
    // Skip the header
    statFile.ignore(numeric_limits<streamsize>::max(), ETX);

    uint8_t type;
    uint32_t length;

    while (statFile) {
        statFile.read(reinterpret_cast<char *>(&type), sizeof(type));
        statFile.read(reinterpret_cast<char *>(&length), sizeof(length));
        length = ntohl(length);

        if (type == _model)
            return length;

        // TODO find out why it's here
        statFile.ignore(length);
    }

    throw runtime_error("The input file contains statistics for a different "
                                "Markov model than specified");
}

int MarkovPassGen::nextPassword(char *buffer)
{
    if (_index >= _lastIndex)
        return -1;

    // Determine the current length
    while(_index >= _permutations[_currentLength])
        _currentLength++;

    // Place the zero value after the password
    buffer[_currentLength] = 0;

    // Create the password
    uint64_t index = _index - _permutations[_currentLength -1];
    uint64_t partialIndex;
    char lastChar = 0;

    for (unsigned p = 0; p < _currentLength; p++) {
        partialIndex = index % _thresholds[p];
        index = index / _thresholds[p];

        lastChar = _markovTable[p * CHARSET_SIZE * _maxThreshold + lastChar *
                                _maxThreshold + partialIndex];

        buffer[p] = lastChar;
    }

    _index++;

    return _currentLength;
}

void MarkovPassGen::details()
{
    cout << "Minimal password length: " << _minLength << "\n";
    cout << "Maximal password length: " << _maxLength << "\n";

    cout << "Thresholds: ";
    for (unsigned i = 0; i < _maxLength; i++) {
        cout << _thresholds[i] << " ";
    }
    cout << "\n";

    cout << "Markov model: ";
    if (_model == Model::CLASSIC)
        cout << "classic";
    else if (_model == Model::LAYERED)
        cout << "layered";
    cout << "\n";

    cout << "Starting index: " << _index << "\n";
    cout << "Last index: " << _lastIndex << "\n";
}

void MarkovPassGen::printMarkovTable()
{
    for (unsigned p = 0; p < _maxLength; p++) {
        cout << "========= Position = " << p << " =========\n";
        for (unsigned i = 0; i < 127; i++) {
            if (i > 0 && i < 32)
                continue;

            cout << (char) i << " | ";
            for (unsigned j = 0; j < _thresholds[p]; j++) {
                char c;
                c = _markovTable[p * CHARSET_SIZE * _maxThreshold + i *
                                 _maxThreshold + j];
                cout << c;
            }
            cout << "\n";
        }
    }
}

void MarkovPassGen::applyMask(MarkovPassGen::SortElement*
table[MAX_PASS_LENGTH][CHARSET_SIZE])
{
    for (unsigned p = 0; p < _maxLength; p++) {
        for (unsigned i = 0; i < CHARSET_SIZE; i++) {
            for (unsigned j = 0; j < CHARSET_SIZE; j++) {
                if (_mask[p].Satisfy(table[p][i][j].nextState))
                    table[p][i][j].probability += UINT16_MAX + 1;
            }
        }
    }
}
