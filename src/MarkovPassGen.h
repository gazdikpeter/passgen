/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    21/02/17
 *  @copyright The MIT License (MIT)
 *
 */

#ifndef PASSGEN_MARKOVPASSGEN_H
#define PASSGEN_MARKOVPASSGEN_H

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include "Constants.h"
#include "Mask.h"
#include "PassGen.h"

#include <cstdint>
#include <fstream>
#include <string>
#include <mutex>

class MarkovPassGen : public PassGen {
public:
    struct Options
    {
        std::string statFile;
        std::string model = "classic";
        std::string thresholds = "5";
        std::string length = "1:64";
        std::string mask;
        bool printTable = false;
    };

    MarkovPassGen(const Options & options);
    virtual ~MarkovPassGen();

    /**
     * Generate the next password into the buffer
     * @param buffer
     * @return The length of the generated password
     */
    int nextPassword(char *buffer);

    /**
     * Print the details about the current state of the password generator
     */
    void details();

    /**
     * Print the current content of the Markov table
     */
    void printMarkovTable();

private:

    /**
     * The data structure for sorting characters by its probability
     */
    struct SortElement
    {
        uint8_t nextState;
        uint32_t probability;
    };

    enum Model
    {
        CLASSIC = 1,
        LAYERED = 2,
    };

private:

    /**
     * Initialize all the necessary data structures for password generating
     */
    void initMemory();

    /**
     * Parse the given options
     * @param options
     */
    void parseOptions(const Options & options);

    static int compareSortElements(const void *p1, const void *p2);

    /**
     * Test if the character is printable ASCII character
     * @param value
     * @return TRUE if the character is printable ASCII character
     */
    static bool isValidChar(uint8_t value);

    /**
     * Calculate the number of all passwords with the given length
     * @param length
     * @return
     */
    uint64_t numOfPasswords(unsigned length);

    /**
     * Skip the header of a stat file
     * @param statFile
     * @return The length of the payload
     */
    unsigned skipHeader(std::ifstream & statFile);

    /**
     * Modify probabilities in the sorting table according to mask
     * @param table
     */
    void applyMask(SortElement *table[MAX_PASS_LENGTH][CHARSET_SIZE]);

    std::string _statFile;
    Mask _mask;
    Model _model;

    /**
     * A 3D Markov table
     */
    char *_markovTable;

    /**
     * The size of the markov table
     */
    unsigned _markovTableSize;
    
    /**
     * The precomputed number of permutations for each length
     */
    std::vector<uint64_t> _permutations;

    /**
     * The number of characters per position
     */
    std::vector<unsigned>  _thresholds;

    unsigned _minLength;
    unsigned _maxLength;
    unsigned _currentLength;
    unsigned _maxThreshold;
    uint64_t _index;
    uint64_t _lastIndex;
};


#endif //PASSGEN_MARKOVPASSGEN_H
