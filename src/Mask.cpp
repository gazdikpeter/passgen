/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    21/02/17
 *  @copyright The MIT License (MIT)
 *
 */


#include <stdexcept>
#include "Mask.h"

using namespace std;

MaskElement::MaskElement()
{
  _charsetFlags.set();
}

MaskElement::MaskElement(const std::string & single_mask)
{
  MaskRangeFunction fn = nullptr;

  // Mask is single character
  if (single_mask.size() == 1)
  {
    _charsetFlags[single_mask[0]] = true;
    return;
  }

  // Assign range function
  for (int i = 0; i < NUM_METACHARS; i++)
  {
    if (RANGE_FUNCTIONS[i].character == single_mask[1])
    {
      fn = RANGE_FUNCTIONS[i].function;
      break;
    }
  }

  // Set bits according to metacharacter
  if (fn == nullptr)
    throw runtime_error { "Mask: Unknown metacharacter" };

  for (int i = 0; i < CHARSET_SIZE; i++)
  {
    _charsetFlags[i] = fn(i);
  }
}

const MaskElement& Mask::operator [](std::size_t idx)
{
  if (idx >=_maskElements.size())
    return _outOfRange;

  return _maskElements[idx];
}

bool MaskElement::Satisfy(uint8_t character) const
{
  return _charsetFlags.test(character);
}

Mask::Mask(const std::string& mask)
{
  for (unsigned i = 0; i < mask.size(); i++)
  {
    if (mask[i] == '?')
    {
      _maskElements.push_back(MaskElement {mask.substr(i, 2)});
      i++;
    }
    else
    {
      _maskElements.push_back(MaskElement {mask.substr(i, 1)});
    }
  }
}

Mask::~Mask()
{
}

std::size_t MaskElement::Count() const
{
  return _charsetFlags.count();
}
