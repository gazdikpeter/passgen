/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    21/02/17
 *  @copyright The MIT License (MIT)
 *
 */


#ifndef PASSGEN_MASK_H
#define PASSGEN_MASK_H

#include "Constants.h"

#include <cstdint>
#include <string>
#include <bitset>
#include <vector>

typedef bool (*MaskRangeFunction)(uint8_t);
struct MaskRange
{
    char character;
    MaskRangeFunction function;
};

const unsigned NUM_METACHARS = 7;
static const MaskRange RANGE_FUNCTIONS[NUM_METACHARS] =
{
        {'l', [] (uint8_t ch) { return ch >= 'a' && ch <= 'z'; } },
        {'u', [] (uint8_t ch) { return ch >= 'A' && ch <= 'Z'; } },
        {'c', [] (uint8_t ch) { return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch < 'Z'); } },
        {'d', [] (uint8_t ch) { return ch >= '0' && ch <= '9'; } },
        {'a', [] (uint8_t ch) { return ch >= ' ' && ch <= '~'; } },
        {'s', [] (uint8_t ch) { return (ch >= ' ' && ch <= '/') || (ch >= ':' && ch <= '@') || (ch >= '[' && ch <= '`') || (ch >= '{' && ch <= '~'); } },
        {'?', [] (uint8_t ch) { return ch == '?'; } }
};

class MaskElement
{
public:
    MaskElement();
    MaskElement(const std::string & single_mask);

    /**
     * Test if given character satisfy the mask
     */
    bool Satisfy(uint8_t character) const;

    /**
     * Returns number of characters that satisfy mask
     */
    std::size_t Count() const;
private:
    std::bitset<CHARSET_SIZE> _charsetFlags;
};

class Mask
{
public:
    Mask(const std::string &mask);
    ~Mask();

    const MaskElement & operator[](std::size_t idx);
private:
    std::vector<MaskElement> _maskElements;
    MaskElement _outOfRange;
};


#endif //PASSGEN_MASK_H
