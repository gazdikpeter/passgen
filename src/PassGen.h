/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    11/03/17
 *  @copyright The MIT License (MIT)
 *
 */

#ifndef PASSGEN_PASSGEN_H
#define PASSGEN_PASSGEN_H


class PassGen
{
public:
    PassGen() {};
    virtual ~PassGen() {};

    /**
     * Generate the next password into the buffer
     * @param buffer
     * @return The length of the generated password
     */
    virtual int nextPassword(char *buffer) = 0;

    /**
     * Print the details about the current state of the password generator
     */
    virtual void details() = 0;
};


#endif //PASSGEN_PASSGEN_H
