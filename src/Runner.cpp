/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    10/03/2017
 *  @copyright The MIT License (MIT)
 *
 */

#include "Runner.h"

#include <iostream>

using namespace std;

Runner::Runner(const Runner::Options &options, PassGen *passGen) :
        _passGen {passGen}
{
}

Runner::~Runner()
{
}

void Runner::run()
{
    char passwordBuffer[MAX_PASS_LENGTH + 1];

    while(_passGen->nextPassword(passwordBuffer) > 0) {
        cout << passwordBuffer << "\n";
    }
}

void Runner::details()
{
    _passGen->details();
}

