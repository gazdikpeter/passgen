/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    10/03/17
 *  @copyright The MIT License (MIT)
 *
 */

#ifndef PASSGEN_RUNNER_H
#define PASSGEN_RUNNER_H

#include "MarkovPassGen.h"

class Runner
{
public:

    struct Options
    {
    };

    Runner(const Options &options, PassGen *passGen);
    virtual ~Runner();

    void details();
    void run();

private:

    PassGen *_passGen;
};


#endif //PASSGEN_RUNNER_H
