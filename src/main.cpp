/**
 *
 *  @author  Peter Gazdik <gazdik@gmx.com>
 *  @date    21/02/17
 *  @copyright The MIT License (MIT)
 *
 */

#include <getopt.h>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <exception>
#include <stdexcept>

#include "Runner.h"

using namespace std;

struct Options : Runner::Options, MarkovPassGen::Options
{
    bool help = false;
    bool verbose = false;

};

const char *helpMsg = "passgen [OPTIONS]\n\n"
		"Informations:\n"
		"   -h, --help              prints this help\n"
		"   -v, --verbose           verbose mode\n"
		"Markov Generator:\n"
		"   -s, --statistics        file with statistics\n"
		"   -t, --thresholds=glob[:pos]\n"
		"                           number of characters per position\n"
		"         - glob - the global value for each position in password\n"
		"         - pos -  positional comma-separated values(overwrites "
		"global value)\n"
		"   -l, --length=min:max    length of password (default 1:64)\n"
		"   -m, --mask              mask\n"
		"   -M, --model             type of Markov model:\n"
		"         - classic - First-order Markov model (default)\n"
		"         - layered - Layered Markov model\n"
		"   -p, --print-table       print the content of the Markov table\n";

const struct option longOptions[] =
		{
				{ "help", no_argument, 0, 'h' },
				{ "verbose", no_argument, 0, 'v' },
				{ "statistics", required_argument, 0, 's' },
				{ "thresholds", required_argument, 0, 't' },
				{ "length", required_argument, 0, 'l' },
				{ "mask", required_argument, 0, 'm' },
				{ "model", required_argument, 0, 'M' },
				{ "print-table", no_argument, 0, 'p' },
				{ 0, 0, 0, 0 }
		};

int main(int argc, char *argv[])
{
    Options options;
	int opt, optionIndex;

	while ((opt = getopt_long(argc, argv, "hvs:t:l:m:M:p", longOptions,
		                      &optionIndex)) != -1) {
		switch (opt) {
			case 'M':
				options.model = optarg;
				break;
			case 'h':
				options.help = true;
				break;
			case 'v':
				options.verbose = true;
				break;
			case 's':
				options.statFile = optarg;
				break;
			case 't':
				options.thresholds = optarg;
				break;
			case 'l':
				options.length = optarg;
				break;
			case 'm':
				options.mask = optarg;
				break;
			case 'p':
				options.printTable = true;
				break;
			default:
				break;
		}
	}

	if (options.help) {
		cout << helpMsg;
		return EXIT_SUCCESS;
	}

	if (options.statFile.empty()) {
		cout << helpMsg;
		return EXIT_FAILURE;
	}

	Runner *runner;
    MarkovPassGen *passGen;

	try {
		passGen = new MarkovPassGen(options);
		runner = new Runner(options, passGen);
	}
	catch (exception &e) {
		cerr << "Error: " << e.what() << endl;
		return EXIT_FAILURE;
	}

	if (options.printTable) {
		passGen->printMarkovTable();
		return EXIT_SUCCESS;
	}

	if (options.verbose) {
		runner->details();
	}

	try {
		runner->run();
	}
	catch (exception &e) {
		cerr << "Error: " << e.what() << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}